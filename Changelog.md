# Changelog


## 1.4
- Canonicalizes file names in torrents before comparisson to filesystem. Some torrents where the folder name was "folder/" and file name was "/file" got the name "folder//file" which made the program list "folder/file" as a candidate for deletion.

## 1.3
- Support for single-file torrents. Should fix the "missing field" errors.

## 1.2
- Now searches recursively through folders. A torrent containing the file b/c.file now matches both ./b/c.file in downloads directory and ./a/b/c.file. This means that you can use this tool with a folder structure of for example "Downloads/Tracker-name/files" on the top level directory and it will still identify files.

## 1.1
- Static version recompiled on Ubuntu 16.04LTS for further backwards compatibility.
