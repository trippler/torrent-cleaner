# TorrentCleaner

TorrentCleaner is a torrent data management tool to identify data in a folder not being referenced by any torrents.

  - Written in C++
  - Simple CLI
  - Fast


## Documentation
Finds all files in a data directory, and adds them to a list. Then parses through all the .torrent files to make a list of files that are referenced. Then prints out a list of the files in the data directory that are not referenced by any torrents.

## Why use this?
Keeping the downloads folder tidy when you have a lot of torrents is a pain. It may contain a lot of files and folders and there isn't a good way to see which files are in use (seeding), and which are safe to delete. If you cross seed, that is download a torrent from one private tracker (A) and see that it is also available on another private tracker (B), so you download the torrent from there as well for seeding this becomes even more of an issue, as you can have many torrents using a single file, and you don't know how many torrents depend on a file you want to delete.

This tool solves this automatically by making a list of the files that are being depended on by one or more torrents, and giving you back a list of the files that aren't depended on by any torrents (and can safely be deleted). This means your torrent client can simply delete the torrent file, and leave the cleanup of the files to this tool.

When you use multiple trackers and cross-seed the same files this becomes even more of a problem.

###Usage

####Installation
It's a standalone executable, so just download the latest version in the **Releases** section, and make it executable
```sh
chmod +x TorrentCleaner-1.*
```
To use the tool, do the following:

```sh
Torrent-Cleaner <data directory> <torrents>
```

An example which will return a list of all files in "/path/to/download" that are not referenced by any torrents in /path/to/torrents/\*.torrent or /path/to/other\_torrents/\*.torrent
```sh
Torrent-Cleaner /path/to/download /path/to/torrents/*.torrent /path/to/other_torrents/*.torrent
```

If you have a "movies" and a "torrents" folder containing
```
movies/Movie1.mpv
movies/Movie2.mpv
movies/Movie3.mpv
torrents/Movie1.mpv.torrent
torrents/Movie2.mpv.torrent
```

Running "./Torrent-Cleaner movies/ torrents/*.torrent" would print out:

```
Movie3.mpv
```
If you now remove Movie1.mpv.torrent and run the command again it will print out:
```
Movie1.mpv
Movie3.mpv
```


To delete these files first cd into the directory and pass all the arguments to rm
```sh
cd /path/to/download
Torrent-Cleaner . /path/to/torrents/*.torrent /path/to/other_torrents/*.torrent | tr '\n' '\0' | xargs -0 -n 1 rm
```

Or to move the files to the /path/to/unreferenced folder:
```sh
cd /path/to/download
Torrent-Cleaner . /path/to/torrents/*.torrent /path/to/other_torrents/*.torrent | tr '\n' '\0' | xargs -0 -n 1 mv -t /path/to/unreferenced/
```


## Releases
[Version 1.4 Linux static](https://bitbucket.org/trippler/torrent-cleaner/downloads/TorrentCleaner-1.4)

## Building from source
```sh
$ git clone  https://trippler@bitbucket.org/trippler/torrent-cleaner.git
$ cd torrent-cleaner
$ qmake TorrentCleaner.pro
$ make
```
Compiling requires Qt.

License
----

GPL v2

bencode and abstracttreeitem classes are GPL v3
