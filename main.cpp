#include <QFile>
#include <QByteArray>
#include <string>
#include <set>
#include <algorithm>
#include <iostream>
#include <stdint.h>
#ifdef linux
#define _XOPEN_SOURCE_EXTENDED 1
#include <ftw.h>
#endif
#include<QDirIterator>
#include <unistd.h>
#include <stdio.h>
#include "bencode.h"

std::string canonicalize_file_name_str(std::string str){
	while(str.find("//") != str.npos){
		str.replace(str.find("//"), 2, "/");
	}
	return str;
}

/*
Unfortunately both QFile, and modern C++ structures were unable to open
files encoded with UTF-16 names, but C-style fopen did.
*/
std::set<std::string> ParseTorrent(const char *filename){
	std::set<std::string> result;
	FILE *fp = fopen(filename, "rb");
	if(!fp){
		std::cout << "Error: Can't open torrent: " << filename << std::endl;
		return result;
	}

	fseek(fp, 0L, SEEK_END);
	int64_t sz = ftell(fp);
	rewind(fp);

	char *buffer = (char*)calloc(sz, 1);
	fread(buffer, sz, 1, fp);
	QByteArray ba = QByteArray::fromRawData(buffer, sz);
	fclose(fp);
	Bencode *bencode = Bencode::fromRaw(ba);
	free(buffer);

	Bencode *info = bencode->child("info");
	if(!info){
		printf("Error with info!\n");
		return result;
	}
	Bencode *name = info->child("name");
	Bencode *files = info->child("files");

	if(files == NULL && name != NULL){
		//Special case where release has only 1 file
		result.insert(name->string().toStdString());
		return result;
	}

	if(!files || !name || !info){
		printf("Error: Missing fields in \"%s\"!", filename);
		return result;
	}
	for(int n = 0; n < files->childCount(); ++n){
		Bencode *child = files->child(n)->child("path");
		std::string path = name->string().toStdString();
		for(int m = 0; m < child->childCount(); ++m){
			path += "/" + child->child(m)->string().toStdString();
		}
		result.insert(std::move(canonicalize_file_name_str(path)));
	}
	return result;
}

std::set<std::string> nftw_paths;
int nftw_path_skip;
int callback(const char* file, const struct stat* sb, int flag, struct FTW* s){
	if(flag == FTW_F)
		nftw_paths.insert(file+nftw_path_skip);
	return FTW_CONTINUE;
}

std::set<std::string> files_in_directory_linux(const char *path){
	nftw_paths.clear();
	nftw_path_skip = strlen(path);
	if(path[nftw_path_skip-1] != '/')
		nftw_path_skip++;
	int fds = getdtablesize() - 10;
	int flags = FTW_PHYS | FTW_ACTIONRETVAL;
	nftw(path, callback, fds, flags);
	return nftw_paths;
}

std::set<std::string> files_in_directory_qt(const char *path){
	std::set<std::string> paths;
	QDirIterator it(path, QStringList() << "*", QDir::Files, QDirIterator::Subdirectories);
	while (it.hasNext())
		paths.insert(it.next().toStdString());
	return paths;
}

std::set<std::string> files_in_directory(const char *path){
#ifdef linux
	return files_in_directory_linux(path);
#endif
	return files_in_directory_qt(path);

}

bool compare_string_by_ending(const std::string &a, const std::string &b){
	int b_length = b.size(), a_length = a.size(), result;
	if(b_length > a_length)
		result = b.compare(b_length-a_length, a_length, a);
	else
		result = a.compare(a_length-b_length, b_length, b);
	return result == 0;
}

std::set<std::string> strings_in_a_and_not_b_by_ending(const std::set<std::string> &a, const std::set<std::string> &b)
{
	std::set<std::string> result;
	for(auto const &x : a)
	{
		bool found = false;
		for(auto const &y : b)
		{
			if(compare_string_by_ending(x, y))
			{
				found = true;
				break;
			}
		}
		if(!found){
			result.insert(x);
		}
	}
	return result;
}

int main(int argc, char *argv[])
{
	if(argc < 3) {
		printf("Usage: %s <data directory> <torrents>\nExample: %s /path/to/data /path/to/torrents/*.torrent\n", argv[0], argv[0]);
		return 0;
	}
	std::set<std::string> torrent_files;
	for(int n = 2; n < argc; ++n){
		auto torrent = ParseTorrent(argv[n]);
		torrent_files.insert(torrent.begin(), torrent.end());
	}
	std::set<std::string> disk_files = files_in_directory(argv[1]);
	std::set<std::string> bad = strings_in_a_and_not_b_by_ending(disk_files, torrent_files);

	for(auto s : bad)
		std::cout << s << std::endl;
}
